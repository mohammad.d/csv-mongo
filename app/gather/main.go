package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"

	"csv/adapter/mongo/item"
	itemDomain "csv/domain/item"
	"csv/internal/config"
	"csv/internal/logger"
)

func main() {
	csvPath := getCsvPathFlag()
	log, err := logger.New("gather")
	if err != nil {
		panic(err)
	}
	defer log.Sync()
	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config.GetByKey("MONGO_URI")))
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	store := item.NewStore(client.Database("myDB"))
	is := itemDomain.NewService(&store)

	app := New(log, is)

	app.Run(csvPath)
}

type App struct {
	log *zap.SugaredLogger
	is  itemDomain.Service
	ch  chan []string
	wg  sync.WaitGroup
}

func New(log *zap.SugaredLogger, is itemDomain.Service) App {
	return App{
		log: log,
		ch:  make(chan []string, 200),
		is:  is,
		wg:  sync.WaitGroup{},
	}
}

func (a *App) readCsv(ctx context.Context, path string) error {
	defer close(a.ch)
	return a.is.ReadItemsFileInChan(ctx, path, a.ch)
}

func (a *App) process(ctx context.Context) {
	a.wg.Add(4)
	for i := 0; i < 4; i++ {
		go func() {
			defer a.wg.Done()
			t := time.NewTicker(time.Second * itemDomain.MinSecInsert)
			batch := make([]itemDomain.RawItem, 0, itemDomain.BatchSize)

			// add batches to database if time limit has reached
			// or batch limit has reached
			for rawData := range a.ch {
				select {
				case <-t.C:
					if len(batch) > 0 {
						err := a.is.InsertBatchWithParse(ctx, batch)
						if err != nil {
							panic(err)
						}
						batch = make([]itemDomain.RawItem, 0, itemDomain.BatchSize)
					}
					t = time.NewTicker(time.Second * itemDomain.MinSecInsert)

				default:
					batch = append(batch, rawData)
					if len(batch) > itemDomain.BatchSize {
						err := a.is.InsertBatchWithParse(ctx, batch)
						if err != nil {
							panic(err)
						}
						batch = make([]itemDomain.RawItem, 0, itemDomain.BatchSize)
						t = time.NewTicker(time.Second * itemDomain.MinSecInsert)
					}
				}
			}
			if len(batch) > 0 {
				err := a.is.InsertBatchWithParse(ctx, batch)
				if err != nil {
					panic(err)
				}
			}
		}()
	}
}

func (a *App) Run(csvPath string) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	go a.readCsv(ctx, csvPath)
	a.process(ctx)

	// graceful shutdown
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	done := make(chan struct{})

	go func() {
		a.wg.Wait()
		done <- struct{}{}
	}()

	select {
	case sig := <-shutdown:
		fmt.Println(sig)
		a.log.Infow("shutdown", "status", "shutdown started", "signal", sig)
		a.log.Infow("shutdown", "status", "shutdown complete", "signal", sig)
		cancel()
		a.wg.Wait()

	case <-done:
		a.log.Infow("shutdown", "status", "done successfully")
	}

}

func getCsvPathFlag() string {
	path := flag.String("path", "", "path of csv file")
	flag.Parse()
	if *path == "" {
		panic(fmt.Errorf("fill path of the file with -path"))
	}
	return *path
}
