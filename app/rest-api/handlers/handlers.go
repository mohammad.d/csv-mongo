package handlers

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	itStore "csv/adapter/mongo/item"
	"csv/app/rest-api/handlers/v1/itemgrp"
	"csv/app/rest-api/mid"
	"csv/domain/item"
)

type APIMuxConfig struct {
	Shutdown chan os.Signal
	Log      *zap.SugaredLogger
	DB       *mongo.Database
}

func v1(app *gin.Engine, cfg APIMuxConfig) {
	const version = "v1"

	iStore := itStore.NewStore(cfg.DB)
	itemService := item.NewService(&iStore)

	ith := itemgrp.Handler{IS: itemService}

	app.Handle(http.MethodGet, fmt.Sprintf("%s/items", version), ith.Query)

}

func APIMux(cfg APIMuxConfig) *gin.Engine {
	r := gin.Default()
	r.Use(mid.Errors())
	v1(r, cfg)
	return r
}
