package itemgrp

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"csv/domain/item"
	"csv/domain/sys/validate"
)

type Handler struct {
	IS item.Service
}

func (h Handler) Query(c *gin.Context) {
	offset := c.DefaultQuery("offset", "0")
	o, err := strconv.Atoi(offset)
	if err != nil {
		err = validate.NewRequestError(fmt.Errorf("invalid offset param"), http.StatusBadRequest)
		c.Error(err)
		return
	}
	limit := c.DefaultQuery("limit", "100")
	li, err := strconv.Atoi(limit)
	if err != nil {
		err = validate.NewRequestError(fmt.Errorf("invalid limit param"), http.StatusBadRequest)
		c.Error(err)
		return
	}

	items, err := h.IS.GetItems(c, o, li)
	if err != nil {
		c.Error(err)
		return
	}
	c.JSON(http.StatusOK, items)
}
