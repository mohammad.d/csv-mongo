package item

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"csv/domain/data/item"
)

const (
	collection = "item"
)

type Store struct {
	db *mongo.Database
}

func NewStore(db *mongo.Database) Store {
	return Store{
		db: db,
	}
}

func (s *Store) BulkInsert(ctx context.Context, items []item.Model) error {
	vs := make([]interface{}, 0, len(items))
	for _, e := range items {
		vs = append(vs, e)
	}
	_, err := s.db.Collection(collection).InsertMany(ctx, vs)
	if err != nil {
		return err
	}
	return nil
}

func (s *Store) Query(ctx context.Context, offset, limit int) ([]item.Model, error) {
	ops := options.Find().SetSkip(int64(offset)).SetLimit(int64(limit))
	cursor, err := s.db.Collection(collection).Find(ctx, bson.M{}, ops)
	if err != nil {
		return nil, err
	}
	var items []item.Model
	err = cursor.All(ctx, &items)
	if err != nil {
		return nil, err
	}
	return items, nil
}
