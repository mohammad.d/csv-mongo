package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

func GetByKey(key string) string {
	r := viper.Get(key)
	if r == nil {
		panic(fmt.Errorf("key %s doesn't exist in .env", key))
	}
	return r.(string)
}

func init() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	viper.SetConfigFile(wd + "/.env")
	err = viper.ReadInConfig()

	if err != nil {
		panic(err)
	}
}
