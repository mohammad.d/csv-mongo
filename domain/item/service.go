package item

import (
	"bufio"
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"os"

	"csv/domain/data/item"
)

type Service struct {
	store IStore
}

func NewService(s IStore) Service {
	return Service{s}
}

const (
	BatchSize    = 100
	MinSecInsert = 1
)

type RawItem []string

// ReadItemsFileInChan reads a csv file and puts it in a channel. it takes context, path to the file and
// a channel where it's going to put the raw data of the item.
func (s Service) ReadItemsFileInChan(ctx context.Context, path string, ch chan []string) error {
	f, err := os.Open(path)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	// skip the titles row
	row1, err := bufio.NewReader(f).ReadString('\n')
	if err != nil {
		panic(err)
	}
	_, err = f.Seek(int64(len(row1)), io.SeekStart)
	if err != nil {
		panic(err)
	}

	r := bufio.NewReader(f)
	csvReader := csv.NewReader(r)
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			return nil
		}

		if err != nil {
			panic(err)
		}
		ch <- row

		select {
		case <-ctx.Done():
			return ctx.Err()

		default:
		}
	}
}

// InsertBatchWithParse inserts batchItem to its repository
func (s Service) InsertBatchWithParse(ctx context.Context, batch []RawItem) error {
	batchItem := make([]item.Model, 0, BatchSize)
	for _, row := range batch {
		m, err := item.ParseStringSlice(row)
		if err != nil {
			panic(err)
		}
		batchItem = append(batchItem, m)
	}
	fmt.Println("added successfully")
	if err := s.store.BulkInsert(ctx, batchItem); err != nil {
		return err
	}
	return nil
}

// GetItems gets Items from repository.
func (s Service) GetItems(ctx context.Context, offset, limit int) ([]item.Model, error) {
	return s.store.Query(ctx, offset, limit)
}
