package item

import (
	"context"
	"csv/domain/data/item"
)

type IStore interface {
	BulkInsert(ctx context.Context, items []item.Model) error
	Query(ctx context.Context, offset, limit int) ([]item.Model, error)
}
