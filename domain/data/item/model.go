package item

import (
	"errors"
	"strconv"
	"time"
)

type Model struct {
	SeriesReference string    `bson:"series_referenceReference"`
	Period          time.Time `bson:"Period"`
	DataValue       *float32  `bson:"Data_value;omitempty"`
	Suppressed      *string   `bson:"Suppressed;omitempty"`
	Status          string    `bson:"STATUS"`
	Units           string    `bson:"UNITS"`
	Magnitude       int32     `bson:"Magnitude"`
	Subject         string    `bson:"Subject"`
	Group           string    `bson:"Group"`
	SeriesTitle1    string    `bson:"Series_title_1"`
	SeriesTitle2    string    `bson:"Series_title_2"`
	SeriesTitle3    string    `bson:"Series_title_3"`
	SeriesTitle4    string    `bson:"Series_title_4"`
	SeriesTitle5    *string   `bson:"Series_title_5;omitempty"`
}

func ParseStringSlice(s []string) (Model, error) {
	if len(s) < 14 {
		return Model{}, errors.New("length of the slice of string is less than 14")
	}
	p, err := time.Parse("2006.01", s[1])
	if err != nil {
		return Model{}, err
	}
	var dv32Ptr *float32
	if s[2] != "" {
		dV, err := strconv.ParseFloat(s[2], 32)
		if err != nil {
			return Model{}, err
		}
		p := float32(dV)
		dv32Ptr = &p
	}

	var suppressedPtr *string
	if s[3] != "" {
		suppressedPtr = &s[3]
	}

	mag, err := strconv.Atoi(s[6])
	if err != nil {
		return Model{}, err
	}

	var st5Ptr *string
	if s[13] != "" {
		st5Ptr = &s[13]
	}

	return Model{
		SeriesReference: s[0],
		Period:          p,
		DataValue:       dv32Ptr,
		Suppressed:      suppressedPtr,
		Status:          s[4],
		Units:           s[5],
		Magnitude:       int32(mag),
		Subject:         s[7],
		Group:           s[8],
		SeriesTitle1:    s[9],
		SeriesTitle2:    s[10],
		SeriesTitle3:    s[11],
		SeriesTitle4:    s[12],
		SeriesTitle5:    st5Ptr,
	}, nil
}
