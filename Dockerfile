FROM golang:1.19 as go
# Copy the source code into the container.
COPY . /service

# Build the rest-api binary.
WORKDIR /service/apps/rest-api

RUN go mod tidy
RUN go mod vendor

RUN go build main.go



FROM alpine:3.14
RUN apk add libc6-compat


COPY --from=go /service/apps/rest-api/main /service/rest-api
COPY .env /service/
WORKDIR /service
CMD ["./rest-api"]
